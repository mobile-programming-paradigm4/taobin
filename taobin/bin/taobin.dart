import 'dart:io';

class Category {
  List rec = ["โกโก้เย็น", "ลาเต้เย็น"];
  List cof = ["ลาเต้", "เอสเพรสโซ่"];
  List tea = ["ชาไทย", "ชาเย็น"];
  List mil = ["โกโก้", "นมคาราเมล"];
  List pro = ["นมโปรตีน", "โกโก้โปรตีน"];
  List sod = ["น้ำมะนาวโซดา", "น้ำลิ้นจี่"];
}

String HotorCool(String sum) {
  print("ต้องการร้อนหรือเย็น \n1 : ร้อน \n2 : เย็น");
  String hoc = stdin.readLineSync()!;
  if(hoc=="1"){
    sum = sum + "ร้อน";
  }else if(hoc=="2"){
    sum = sum + "เย็น";
  }
  return sum;
}

String SweetnessLevel(String sum) {
  print("ต้องการความหวานเท่าไร \n1 : หวานน้อย \n2 : หวานปกติ \n3 : หวานมาก");
  String sweetnessLevel = stdin.readLineSync()!;
  if(sweetnessLevel=="1"){
    sum = sum + " หวานน้อย";
    sum = Accessory(sum);
  }else if(sweetnessLevel=="2"){
    sum = sum + " หวานปกติ";
    sum = Accessory(sum);
  }else if(sweetnessLevel=="3"){
    sum = sum + " หวานมาก";
    sum = Accessory(sum);
  }
  return sum;
}

String Accessory(String sum) {
  print("รับหลอดหรือฝาเพิ่มหรือไม่ \n1 : รับหลอด \n2 : รับฝา \n3 : รับหลอดและฝา \n4 : ไม่รับอะไรเลย");
  String accessory = stdin.readLineSync()!;
  if(accessory=="1"){
    sum = sum + " พร้อมหลอด";
  }else if(accessory=="2"){
    sum = sum + " พร้อมฝา";
  }else if(accessory=="3"){
    sum = sum + " พร้อมหลอดและฝา";
  }else if(accessory=="4"){
    sum = sum + " ไม่รับหลอดและฝา";
  }
  return sum;
}

String Category1(String sum) {
  print(Category().rec);
  String product = stdin.readLineSync()!;
  if(product=="1"){
    sum = sum + Category().rec[0];
    sum = SweetnessLevel(sum);
  }else if(product=="2"){
    sum = sum + Category().rec[1];
    sum = SweetnessLevel(sum);
  }
  return sum;
}

String Category2(String sum) {
  print(Category().cof);
  String product = stdin.readLineSync()!;
  if(product=="1"){
    sum = sum + Category().cof[0];
    sum = HotorCool(sum);
    sum = SweetnessLevel(sum);
  }else if(product=="2"){
    sum = sum + Category().cof[1];
    sum = HotorCool(sum);
    sum = SweetnessLevel(sum);
  }
  return sum;
}

String Category3(String sum) {
  print(Category().tea);
  String product = stdin.readLineSync()!;
  if(product=="1"){
    sum = sum + Category().tea[0];
    sum = HotorCool(sum);
    sum = SweetnessLevel(sum);
  }else if(product=="2"){
    sum = sum + Category().tea[1];
    sum = HotorCool(sum);
    sum = SweetnessLevel(sum);
  }
  return sum;
}

String Category4(String sum) {
  print(Category().mil);
  String product = stdin.readLineSync()!;
  if(product=="1"){
    sum = sum + Category().mil[0];
    sum = HotorCool(sum);
    sum = SweetnessLevel(sum);
  }else if(product=="2"){
    sum = sum + Category().mil[1];
    sum = HotorCool(sum);
    sum = SweetnessLevel(sum);
  }
  return sum;
}

String Category5(String sum) {
  print(Category().pro);
  String product = stdin.readLineSync()!;
  if(product=="1"){
    sum = sum + Category().pro[0];
    sum = HotorCool(sum);
    sum = SweetnessLevel(sum);
  }else if(product=="2"){
    sum = sum + Category().pro[1];
    sum = HotorCool(sum);
    sum = SweetnessLevel(sum);
  }
  return sum;
}

String Category6(String sum) {
  print(Category().sod);
  String product = stdin.readLineSync()!;
  if(product=="1"){
    sum = sum + Category().sod[0];
    sum = HotorCool(sum);
    sum = SweetnessLevel(sum);
  }else if(product=="2"){
    sum = sum + Category().sod[1];
    sum = HotorCool(sum);
    sum = SweetnessLevel(sum);
  }
  return sum;
}

void SelectCategoty(String category, String sum) {
  if (category == "1") {
    sum = Category1(sum);
    print(sum);
  } else if (category == "2") {
    sum = Category2(sum);
    print(sum);
  } else if (category == "3") {
    sum = Category3(sum);
    print(sum);
  } else if (category == "4") {
    sum = Category4(sum);
    print(sum);
  } else if (category == "5") {
    sum = Category5(sum);
    print(sum);
  } else if (category == "6") {
    sum = Category6(sum);
    print(sum);
  } 
}

void main(List<String> arguments) {
  String sum = "";
  stdout.write("Choose category : \n");
  print("1 : เมนูแนะนำ \n2 : กาแฟ \n3 : ชา \n4 : นม โกโก้ และคาราเมล \n5 : โปรตีนเชค \n6 : โซดา และอื่นๆ");
  String category = stdin.readLineSync()!;
  SelectCategoty(category, sum); 
}
